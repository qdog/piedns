# Pihole External DNS (PieDNS)
The app will listen to new ingress hosts and create the corresponding dns record n pihole


to install using helm first add the repo
```shell
helm repo add https://gitlab.com/api/v4/projects/35593509/packages/helm/stable
helm repo update
```
create a values.yaml file
```yaml
pihole:
  url: http://pihole-web.network  # the url to the admin interface for pihole in the cluster
  apiSecretName: pihole-api-key  # the secret name containing the api token in /admin/settings.php?tab=api
  apiSecretKey: api-key  # the value of that secret
```
you can create a simple secret via
```shell
kubectl create secret generic pihole-api-key --from-literal=api-key=...
```

to install the helm chart run
```shell
helm install piedns piedns/piedns --values values.yaml
```
