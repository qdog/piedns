from __future__ import annotations

import logging
import signal
import time
from dataclasses import dataclass

import click
import jq
import requests
from kubernetes import config, client, watch
from kubernetes.client import V1Ingress, V1LoadBalancerIngress, V1IngressRule

logger = logging.getLogger(__name__)

signal.signal(signal.SIGTERM, lambda *_: signal.raise_signal(signal.SIGINT))


def prepare_logger(level: str) -> None:
    ch = logging.StreamHandler()
    if level == "DEBUG":
        log_format = logging.Formatter(
            "[%(asctime)s - %(name)s - %(levelname)s - lineno:%(lineno)d]: %(message)s"
        )
    else:
        log_format = logging.Formatter(
            "[%(asctime)s - %(name)s - %(levelname)s]: %(message)s"
        )
    ch.setFormatter(log_format)
    logger.addHandler(ch)
    logger.setLevel(level)


@dataclass
class ADNSRecord:
    host: str
    target: str


class PiHoleHandler:
    # https://github.com/pi-hole/AdminLTE/blob/master/api.php
    def __init__(
        self,
        pihole_url: str,
        api_token: str,
    ) -> None:
        logger.info(f"{pihole_url=}")
        self.pihole_url = pihole_url
        self.api_token = api_token

    def send_request(self, action: str, dns_record: ADNSRecord):
        for i in range(3):
            try:
                r = requests.get(
                    url=f"{self.pihole_url}/admin/api.php?customdns",
                    params={
                        "action": action,
                        "domain": dns_record.host,
                        "ip": dns_record.target,
                        "auth": self.api_token,
                    },
                    timeout=10,
                )
                logger.info(f"{r.status_code=} {r.content=}")
                break
            except requests.exceptions.ConnectionError:
                logger.info("could not connect to the pihole api, will try again")
                time.sleep(1)
        else:
            logger.error("could not connect to the pihole api")

    def add(self, dns_record: ADNSRecord) -> None:
        self.send_request("add", dns_record)

    def delete(self, dns_record: ADNSRecord) -> None:
        self.send_request("delete", dns_record)

    def modify(self, dns_record: ADNSRecord) -> None:
        self.send_request("delete", dns_record)
        self.send_request("add", dns_record)


class Selector:
    def __init__(self, jq_query: str) -> None:
        self.jq_c = jq.compile(jq_query)

    def eval(self, value: dict | list | str | int | float | None) -> bool:
        logger.debug(f"evaluating on [{self.jq_c.program_string}]")
        r = self.jq_c.input(value).first()
        logger.debug(f"resulting in {r}")
        return bool(r)


selector_str_list = [
    "(.spec.rules | length) > 0",
    '.spec.rules | map(.http.paths | map(.path == "/")) | flatten | any',
    ".status.loadBalancer.ingress != null",
]


def ingress_to_adns_record(ingress: V1Ingress) -> ADNSRecord:
    rule: V1IngressRule
    host: str | None = None
    for rule in ingress.spec.rules:
        for path in rule.http.paths:
            if path.path == "/":
                host = rule.host
    assert host is not None
    load_balancer_ingress: list[
        V1LoadBalancerIngress
    ] = ingress.status.load_balancer.ingress
    logger.debug(f"{load_balancer_ingress=}")

    dns_record = ADNSRecord(host=host, target=load_balancer_ingress[0].ip)
    logger.debug(f"{dns_record=}")
    return dns_record


@click.command()
@click.option("--pihole-url", envvar="PIHOLE_URL", required=True)
@click.option("--pihole-api-token", envvar="PIHOLE_API_TOKEN", required=True)
@click.option("--select-rule", multiple=True)
@click.option(
    "--log-level",
    envvar="LOG_LEVEL",
    type=click.Choice(["ERROR", "WARNING", "INFO", "DEBUG"], case_sensitive=False),
    default="INFO",
)
def main(
    pihole_url: str, pihole_api_token: str, select_rule: tuple[str], log_level: str
) -> int:
    prepare_logger(log_level.upper())
    logger.info(f"{select_rule=}")
    config.load_config()
    v1 = client.NetworkingV1Api()
    w = watch.Watch()
    selector_list = [Selector(q) for q in selector_str_list + list(select_rule)]

    phh = PiHoleHandler(
        pihole_url=pihole_url,
        api_token=pihole_api_token,
    )

    try:
        for r in w.stream(v1.list_ingress_for_all_namespaces):
            event_type: str = r["type"]
            ingress: V1Ingress = r["object"]
            logger.debug(f"received an ingress event {event_type=}")
            if all(map(lambda s: s.eval(r["raw_object"]), selector_list)):
                dns_record = ingress_to_adns_record(ingress)
                if event_type == "ADDED":
                    logger.info("a new ingress was added")
                    phh.add(dns_record)
                elif event_type == "DELETED":
                    logger.info("an ingress was deleted")
                    phh.delete(dns_record)
                elif event_type == "MODIFIED":
                    phh.modify(dns_record)
            else:
                logger.info("ingress event filtered")

    except KeyboardInterrupt:
        logger.info("leaving watching loop")
        w.stop()

    except Exception:
        logger.exception("got an unexpected error, existing")
        return 1

    logger.info("existing")
    return 0
