FROM python:3.9.12-alpine as base

ENV PYTHONFAULTHANDLER=1 \
    PYTHONHASHSEED=random \
    PYTHONUNBUFFERED=1

FROM base as builder

ENV PIP_DEFAULT_TIMEOUT=100 \
    PIP_DISABLE_PIP_VERSION_CHECK=1 \
    PIP_NO_CACHE_DIR=1 \
    POETRY_VERSION=1.1.11

RUN apk add --no-cache gcc libffi-dev musl-dev python3-dev make
RUN pip install "poetry==$POETRY_VERSION"
RUN python -m venv /venv
ENV PATH /venv/bin:$PATH

COPY pyproject.toml poetry.lock ./
RUN poetry install --no-dev --no-root

COPY . .
RUN poetry build && pip install dist/*.whl

FROM base as final

COPY --from=builder /venv /venv
ENV PATH /venv/bin:$PATH
ENTRYPOINT ["piedns"]
